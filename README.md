# La red social

En una red social como Twitter, cada participante escoge a quién seguir, para
estar al tanto de las publicaciones de esas personas. Esas relaciones pueden
representarse en una matriz binaria de conexiones, en donde cada fila y cada
columna representa las personas en la red social, y en cada posición (i,j) se
almacena un valor de cero (0) si la persona i no sigue a la persona j, o un
valor de uno (1) si la persona i sigue a la persona j. Para una red social de
ejemplo que tiene 6 participantes (Ana, Beto, Carla, Diego, Emilia, Felipe), su
matriz de conexiones podría ser la siguiente:

```text
      |Ana|Beto|Carla|Diego|Emilia|Felipe|
Ana   |  0|   1|    0|    1|     0|     0|
Beto  |  0|   0|    1|    1|     0|     1|
Carla |  1|   1|    0|    1|     1|     1|
Diego |  0|   0|    0|    0|     0|     0|
Emilia|  0|   1|    0|    1|     0|     1|
Felipe|  1|   0|    1|    1|     0|     0|
```

Esta matriz indica que Ana sigue las publicaciones de Beto, pero Beto no sigue
las publicaciones de Ana. En cambio, Carla y Beto sí se siguen mutuamente;
mientras que Ana y Emilia no se conocen entre sí. Se puede notar que la
diagonal principal de la matriz sólo tiene valores de cero (0), pues cada
participante no tiene la opción de seguirse a sí mismo. Teniendo en cuenta
esto, para cualquier red social de n participantes, proponga:

1. La(s) estructura(s) de datos que permiten modelar la red social.

   ## Refinamiento

   ### Estructura: `RedSocial`

   - `nombs`: arreglo de cadenas de caracteres de tamaño `n`.
   - `conns`: matriz de tamaño `n` por `n`.

1. Una función que reciba la red social y determine los nombres de los tuiteros
   activos, es decir, aquellas personas cuya cantidad de seguidores y de
   personas que siguen sume al menos n. En la red social de ejemplo, Beto,
   Carla y Felipe son tuiteros activos.

   ## Refinamiento

   ### Función: `usrActivos`

   #### Entradas

   - `rSocial`: red social con nombres e interconexiones.
   - `n`: tamaño de la red social.

   #### Salidas

   - Ninguna (impresión en pantalla).

   #### Pseudo-código

   1. Para `i` desde `0`; hasta `i` menor a `n`; incremento `i` en 1:
      1. Declaro variable `inter` para contar cantidad de interacciones por
         usuario e inicializo a `0`.
      1. Para `j` desde `0`; hasta `j` menor a `n`; incremento `j` en 1:
         1. Sumo a `inter` seguimientos de fila `i`.
         1. Sumo a `inter` seguidores de fila `j`.
         1. Si valor de `inter` es mayor igual a `n`:
            1. Imprimo nombre de usuario en posición `i`.
            1. Finalizo ciclo.

1. Una función que reciba la red social y tres nombres de participantes y
   determine si éstos conforman un trío de seguimiento, es decir, un conjunto
   de tres personas que se siguen en un sólo sentido. En la red social de
   ejemplo, Ana, Beto y Felipe forman un trío de seguimiento, pues Ana sigue a
   Beto, Beto sigue a Felipe y Felipe sigue a Ana; pero no al contrario (es
   decir, Ana no sigue a Felipe, Felipe no sigue a Beto y Beto no sigue a Ana).

   ## Refinamiento

   ### Función: `esTrioSeg`

   Identifica si tres usuarios se siguen en un sólo sentido.

   #### Entradas

   - `rSocial`: red social con nombres e interconexiones.
   - `usr1`: posición de usuario 1.
   - `usr2`: posición de usuario 2.
   - `usr3`: posición de usuario 3.

   #### Salidas

   - Verdadero si los tres usuarios se siguen en un sólo sentido.

   #### Pseudo-código

   1. Declaro arreglo `usrs` de tamaño `3` con las posiciones de los usuarios.
   1. Para `i` desde `0`; hasta `i` menor a `3`; incremento `i` en 1:
      1. Declaro variable `intFil` para contar cantidad de interacciones por
         fila e inicializo a `0`.
      1. Declaro variable `intCol` para contar cantidad de interacciones por
         columna e inicializo a `0`.
      1. Para `j` desde `0`; hasta `j` menor a `3`; incremento `j` en 1:
         1. Sumo interacciones de fila a `intFil` de usuario con posición
            `usrs[i]`.
         1. Sumo interacciones de columna a `intCol` de usuario con posición
            `usrs[i]`.
      1. Si `intFil` o `intCol` es diferente de `1`:
         1. Retorno falso.
   1. Retorno verdadero.

# Círculos

Un círculo es una figura geométrica cuyos puntos de borde están todos a la
misma distancia del centro, esta distancia es considerada el radio del círculo.
En el plano cartesiano, un círculo puede representarse con 3 valores (como se
muestra en la figura): el radio, la coordenada x de su centro y la coordenada y
de su centro. Ejemplos de círculo:

- Círculo 0: centro en `(3,3)`, radio `3`.
- Círculo 1: centro en `(7,7)`, radio `2`.
- Círculo 2: centro en `(8,3)`, radio `3`.

1. Proponga la estructura que permite representar un círculo con su
   información básica.

   ## Refinamiento

   ### Estructura: `circulo`

   - `x`: coordenada x en el plano cartesiano del centro del círculo.
   - `y`: coordenada y en el plano cartesiano del centro del círculo.
   - `r`: radio del círculo.

2. Proponga una función que reciba dos círculos diferentes e informe si: a) los
   círculos se intersectan; b) los círculos son iguales o no en términos de su
   área. Para la figura de ejemplo:

   - los círculos 0 y 1 no se intersectan; y el círculo 0 es mayor en área que
     el 1.
   - los círculos 0 y 2 se intersectan; y son iguales en área.
   - los círculos 1 y 2 se intersectan; y el círculo 1 es menor en área que el
     2.

   ## Refinamiento

   ### Función: `compararCirculos`

   Compara dos círculos por área e intersección en el plano cartesiano.

   #### Entradas

   - `a`: círculo con coordenadas y radio.
   - `b`: círculo con coordenadas y radio.

   #### Salidas

   - Ninguna (impresión en pantalla).

   #### Pseudo-código

   1. Resto vectores entre puntos de círculos `a` y `b`.
   1. Obtengo magnitud de vector resultante.
   1. Si suma de radios de los círculos es menor a la magnitud del vector
      resultante:
      1. Imprimo: "Círculos no se interceptan".
   1. De lo contrario:
      1. Imprimo: "Círculos se interceptan".
   1. Obtengo área de círculo `a`.
   1. Obtengo área de círculo `b`.
   1. Si área de círculo `a` es mayor que `b`:
      1. Imprimo: "El círculo a es mayor en área que b".
   1. Si área de círculo `b` es mayor que `a`:
      1. Imprimo: "El círculo b es mayor en área que a".
   1. De lo contrario:
      1. Imprimo: "Los círculos son iguales en área".
