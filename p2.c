#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159

typedef struct{
	int x;
	int y;
	unsigned r;
} Circulo;

void compararCirculos(Circulo a, Circulo b);

int main(void){
	Circulo a, b;

	puts("### Ejemplo 1 ###");

	a.x = 3;
	a.y = 3;
	a.r = 3;

	b.x = 7;
	b.y = 7;
	b.r = 2;

	compararCirculos(a,b);

	puts("\n### Ejemplo 2 ###");

	a.x = 3;
	a.y = 3;
	a.r = 3;

	b.x = 8;
	b.y = 3;
	b.r = 3;

	compararCirculos(a,b);

	puts("\n### Ejemplo 3 ###");

	a.x = 7;
	a.y = 7;
	a.r = 2;

	b.x = 8;
	b.y = 3;
	b.r = 3;

	compararCirculos(a,b);

	exit(EXIT_SUCCESS);
}

void compararCirculos(Circulo a, Circulo b){
	int rX, rY;
	double mag;

	rX = a.x - b.x;
	rY = a.y - b.y;

	mag = sqrt(pow(rX, 2) + pow(rY, 2));

	if (a.r + b.r < mag){
		puts("Círculos NO se interceptan.");
	}
	else{
		puts("Círculos se interceptan.");
	}

	double areaA, areaB;

	areaA = PI * pow(a.r, 2);
	areaB = PI * pow(b.r, 2);

	if (areaA > areaB){
		puts("El círculo a es mayor en área que b.");
	}
	else if (areaB > areaA){
		puts("El círculo b es mayor en área que a.");
	}
	else {
		puts("Los círculos son iguales en área.");
	}
}
