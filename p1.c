#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define CANT_PART 6

typedef struct{
	const char * nombs[CANT_PART];
	bool conns[CANT_PART][CANT_PART];
} RedSocial;

void usrActivos(RedSocial rSocial, unsigned n);
bool esTrioSeg(RedSocial rSocial, unsigned usr1, unsigned usr2, unsigned usr3);

int main(void){

	RedSocial twitter = {
		{"Ana", "Beto", "Carla", "Diego", "Emilia", "Felipe"},
		{
			{0, 1, 0, 1, 0, 0},
			{0, 0, 1, 1, 0, 1},
			{1, 1, 0, 1, 1, 1},
			{0, 0, 0, 0, 0, 0},
			{0, 1, 0, 1, 0, 1},
			{1, 0, 1, 1, 0, 0}
		}
	};

	puts("Usuarios activos:");
	usrActivos(twitter, CANT_PART);

	puts("\nTriángulo de seguimiento:"); 
	puts("Ejemplo1:"); 
	printf("Entre %s, %s y %s: ", twitter.nombs[0], twitter.nombs[1],
			twitter.nombs[5]);
	esTrioSeg(twitter, 0, 1, 5) ? printf("%s", "sí\n") :
		printf("%s", "no\n");

	puts("Ejemplo2:"); 
	printf("Entre %s, %s y %s: ", twitter.nombs[1], twitter.nombs[3],
			twitter.nombs[4]);
	esTrioSeg(twitter, 1, 3, 4) ? printf("%s", "sí\n") :
		printf("%s", "no\n");

	exit(EXIT_SUCCESS);
}

void usrActivos(RedSocial rSocial, unsigned n){
	for (size_t i = 0;i < n; i++){
		unsigned inter = 0;
		for (size_t j = 0;j < n; j++){
			inter += rSocial.conns[i][j];
			inter += rSocial.conns[j][i];
			if (inter >= n){
				puts(rSocial.nombs[i]);
				break;
			}
		}
	}
}

bool esTrioSeg(RedSocial rSocial, unsigned usr1, unsigned usr2, unsigned usr3){
	unsigned usrs[3] = {usr1, usr2, usr3};

	for (size_t i = 0; i < 3; i++){
		unsigned intFil = 0;
		unsigned intCol = 0;

		for (size_t j = 0; j < 3; j++){
			intFil += rSocial.conns[usrs[i]][usrs[j]];
			intCol += rSocial.conns[usrs[j]][usrs[i]];
		}

		if (!intFil || !intCol){
			return false;
		}
	}

	return true;
}
