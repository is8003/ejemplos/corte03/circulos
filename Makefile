.PHONY: clean

CC = gcc
CFLAGS = -std=c17 -pedantic -Wall -Wextra -g

OUTDIR = ./bin

all: p1 p2

p1: p1.c
	$(CC) $(CFLAGS) $^ -o $(OUTDIR)/$@

p2: p2.c
	$(CC) $(CFLAGS) -lm $^ -o $(OUTDIR)/$@

clean :
	rm -fv $(OUTDIR)/*
